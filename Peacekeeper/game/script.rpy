﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

image bg mainmenu = "images/bg_mainmenu.png"
image bg boot = "images/bg_boot.png"
image bg wakeup = "images/bg_wakeup.png"
image bg hallway = "images/bg_hallway.png"
image bg static = "images/bg_static.png"
image bg briefing = "images/bg_briefing.png"
image bg admiral = "images/bg_admiral.png"
image bg bridge = "images/bg_bridge.png"
image bg lightspeed = "images/bg_lightspeed.png"
image bg tutorial = "images/bg_tutorial.png"
image bg space = "images/bg_space.png"
image bg hanger = "images/bg_hanger.png"
image bg descent = "images/bg_descent.png"
image bg landing = "images/bg_landing.png"
image bg tunnel = "images/bg_tunnel.png"
image bg village = "images/bg_village.png"
image bg office = "images/bg_office.png"
image bg office nice = "images/bg_office_nice.png"
image bg plateau = "images/bg_plateau.png"
image bg door = "images/bg_door.png"
image bg rebel hallway = "images/bg_rebel_hallway.png"
image bg revelation = "images/bg_revelation.png"
image bg comms proph = "images/bg_comms_proph.png"
image bg comms admin = "images/bg_comms_admin.png"
image bg galaxy = "images/bg_galaxy.png"
image bg eris = "images/bg_eris.png"
image bg eunomia = "images/bg_eunomia.png"
image bg splash = "images/bg_splash.png"

image carrier = "images/ch_carrier.png"
image destroyer = "images/ch_destroyer.png"
image dropship = "images/ch_dropship.png"
image helmsman = "images/ch_helmsman.png"
image engineer = "images/ch_engineer.png"
image firstofficer = "images/ch_firstofficer.png"
image prophetess = "images/ch_prophetess.png"
image prophetess falling = "images/ch_prophetess_falling.png"
image explosion = "images/ef_explosion.png"
image admin = "images/ch_admin.png"
image admin shoot = "images/ch_admin_shoot.png"
image guard1 = "images/ch_androidguard.png"
image guard2 = "images/ch_androidguard.png"
image angry = "images/ch_angry.png"
image angry falling = "images/ch_angry_falling.png"
image angry dead = "images/ch_angry_dead.png"
image soldier = "images/ch_ipfguards.png"
image rebel1 = "images/ch_rguard1.png"
image rebel2 = "images/ch_rguard2.png"
image rebel1 falling = "images/ch_rguard2_falling.png"
image rebel1 shoot = "images/ch_rguard2_shoot.png"
image mainship = "images/ch_mainship.png"
image shell = "images/ch_shell.png"
image beam = "images/ch_beam.png"
image shields = "images/ch_shields.png"

# Transitions
define flash = Fade(0.1, 0.1, 1.0, color="#fff")

# Declare characters used by this game.
define player = Character('[pname]', color="#090")
define fo = Character('First Officer Ngan Huynh', color="#50B")
define en = Character('Engineer Travis Martin', color="00B")
define ad = Character('\"The Admiral\"', color="AAA")
define cm = Character('Ship Computer', color="FFF")
define hm = Character('Helmsman John Robinson', color="0AA")
define gv = Character('Administrator Victor Ladner', color="AA0")
define mb = Character('Mob', color="A00")
define wh = Character('Citizens Whispering', color="A00")
define pv = Character('Private', color="44F")
define pl = Character('Rebel Patrol Leader', color="F00")
define sc = Character('Rebel Second in Command', color="FF0")
define pr = Character('Danielle Kennon', color="F0F")

transform middle:
    xalign 0.5 yalign 0.5

# The game starts here.
init python:

    def show_health(name, hp, maxhp, **properties):

        ui.frame(xfill=False, yminimum=None, **properties)

        ui.vbox() # name from ("HP", bar)

        ui.text(name, size=20)

        ui.hbox() # "HP" from bar
        ui.text("Hull: ", size=20)
        ui.bar(maxhp, hp, xmaximum=150)
        ui.text("%d/%d" % (hp, maxhp), xalign=0.5, size=20)
        ui.close()

        ui.close()

    def show_health_and_shields(name, hp, maxhp, shields, maxshields, **properties):
        ui.frame(xfill=False, yminimum=None, **properties)

        ui.vbox() # name from ("HP", bar)

        ui.text(name, size=20)

        ui.hbox() # "HP" from bar
        ui.text("Hull:      ", size=20)
        ui.bar(maxhp, hp, xmaximum=150)
        ui.text("%d/%d" % (hp, maxhp), xalign=0.5, size=20)
        ui.close()

        ui.hbox() # "HP" from bar
        ui.text("Shields: ", size=20)
        ui.bar(maxshields, shields, xmaximum=150)
        ui.text("%d/%d" % (shields, maxshields), xalign=0.5, size=20)
        ui.close()

        ui.close()

    def damage(base):
        return renpy.random.randint(base / 2, base)

    def chance(c):
        return renpy.random.random() < c;

    class Ship:
        def __init__(self, health, name, hpos):
            self.max_health = health
            self.health = health
            self.name = name
            self.hpos = hpos

        def damage(self, base):
            self.health -= damage(base)
            if self.health < 0:
                self.health = 0

        def destroyed(self):
            return self.health <= 0

        def show_health(self):
            show_health(self.name, self.health, self.max_health, xalign=self.hpos, yalign=0.0)

    class PlayerShip(Ship):
        def __init__(self, health, shields, name):
            Ship.__init__(self, health, name, 0.0)
            self.max_shields = shields
            self.shields = shields

        def damage(self, base):
            if self.shields > 0:
                self.shields -= damage(base * difficulty)
                if self.shields < 0:
                    self.shields = 0
            else:
                Ship.damage(self, base * difficulty)


        def attack(self, target, weapon):
            renpy.call(weapon, target)

        def show_health(self):
            show_health_and_shields(self.name, self.health, self.max_health, self.shields, self.max_shields, xalign=self.hpos, yalign=0.0)

    class EnemyShip(Ship):
        def __init__(self, health, name, main_attack):
            Ship.__init__(self, health, name, 1.0)
            self.main_attack = main_attack

        def attack(self, target):
            renpy.call(self.main_attack, target)


    difficulty = 3;

label splashscreen:
    scene bg splash with dissolve
    play music "mus_title.ogg"
    pause 1.0
    scene black with dissolve
    pause 0.5
    scene bg mainmenu with dissolve

    return

label start:
    $morality = 0
    $pname = "C-1F9EC8A4"
    $sname = "Player Ship"
    $security_team = True
    $fo_alive = True
    $player_pos = Position(xalign = 0.0, yalign = 0.5)
    $enemy_pos = Position(xalign = 1.0, yalign = 0.5)
    $evade = False
    $pods_destroyed = False

    $player_ship = PlayerShip(1000, 1000, "Player")
    $transport_ship = EnemyShip(2000, "Transport", "enemy_beams")
    $admin_ship = EnemyShip(5000, "Administrator", "enemy_shells")

    jump scene_wakeup


label scene_wakeup:
    scene bg space
    menu:
        "What type of robot are you? (choose difficulty)"
        "Revision 0":
            $difficulty = 1
        "Mass Produced":
            $difficulty = 2
        "Experimental Design":
            $difficulty = 3
    play music "mus_ship.ogg"
    scene bg boot
    fo "How much longer will it take for him to boot up?"
    en "Give it some time, princess!"
    en "With an AI system as complex as this, it'll take a while."
    en "Though, according to my calculations, our new captain should have its basic programming running--"
    scene bg wakeup with flash
    show firstofficer at Position(xpos = 0.4, xanchor=0.5, ypos=0.5, yanchor=0.5)
    show engineer at Position(xpos = 0.6, xanchor=0.5, ypos=0.5, yanchor=0.6) behind firstofficer
    player "SYSTEM LAUNCH COMPLETE"
    en "--right about now."
    fo "Welcome aboard, sir! The ship is fully outfitted and ready for your command."
    fo "If you'll follow me, I'll show you to the briefing room."
    en "Hey, calm yourself! I know you're excited that our new captain's booted up, but only its most basic functions are online."
    en "You need to give it some time to acclimate before it speaks with the Admiral."
    fo "You know, it's rude to talk to somebody who is right there! Isn't that right, Captain?"
    menu:
        "Officers should respect their captain.":
            $morality -= 1
            fo "Of course, sir! We meant no disrespect."
        "As an android, I am not offended.":
            $morality += 1
            fo "Maybe not, but, as our captain, we should respect you."

    fo "You see, Travis? He's working perfectly fine!"
    en "Yeah? Well we should still wait a bit before sending it to see the Admiral."
    en "I'll let my life rest in its hands once it's fully operational."
    fo "Well, we might as well show you around the ship."
    en "Go on ahead. I have other matters to handle."
    fo "Whatever. I'm a better tour guide than you, anyways."
    en "I'd tell you to have fun or something, but I'm sure you would anyways. You always had a fascination with androids."
    show engineer:
        xpos 0.6
        ypos 0.5
        linear 0.5 xpos 0.7 ypos 0.6 zoom 1.5
        linear 0.5 xpos 0.9 ypos 0.7 zoom 2.0 alpha 0.0
    fo "Well, now that he's gone, I might as well show you around."
    fo "After all, what good is a captain if he doesn't even know his own ship, right?"
    player "I am preloaded with all relevant schematics and operational parameters."
    en "Oh yeah? Tell us about the ship then."
    $sname = renpy.input("Name your ship.").strip()
    $player_ship.name = sname
    player "Model: Chronos Corporation dreadnought, class C, revision 5.6."
    player "Designation: [sname]."
    player "Armaments: 6-meter high-explosive cannon. Twin 50-megawatt particle beams. Heavy 200-gigawatt particle beam mounted on bottom."
    player "Propulsion: Twin plasma thrusters and a solar sail."
    player "Defenses: electromagnetic shielding can absorb up to 500 megajoules before recharging. Average armor thickness is 3 meters."
    fo "Sounds like you know your stuff. Well, I at least need to lead you to the briefing room and the bridge."
    player "Acknowledged."
    scene bg hallway with fade
    show firstofficer at center
    show helmsman at Position(xalign=0.3, yalign=0.5) behind firstofficer with moveinright
    hm "Sir...captain..."
    hm "I'll get back to my post..."
    hide helmsman with moveoutleft
    fo "Oh! That reminds me. The crew might be a bit uncomfortable with an android as their captain."
    fo "Perhaps it would help if you had a name?"
    menu:
        "My designation is C-1F9EC8A4. No further identification is required.":
            fo "Ok. Whatever you say, captain."
            fo "We're almost there now."

        "That is logical.":
            fo "Great! I'll call you..."
            $pname = renpy.input("What is your name?").strip()
            fo "[pname]! That suits you nicely!"
            player "Acknowledged."
            fo "Great! We're almost there now."

    jump scene_briefing

label scene_briefing:
    scene bg briefing with fade
    show firstofficer at center
    fo "Here we are, the briefing room."
    fo "You'll receive mission details from the Admiral here."
    fo "Our first mission briefing is ready for you."
    fo "I'll stand outside for a moment."
    scene bg_static
    scene bg admiral with pixellate
    play music "mus_admiral.ogg"
    ad "Ahh, our new captain."
    ad "One of the new command models?"
    ad "Most impressive!"
    ad "Not a hint of weakness. If only we had more soldiers unafraid to do the right thing."
    ad "But I digress. We have work to do, you and I, and I won't waste any more of your time."
    ad "Satellites have detected a pair of weapon transports bound for the mining colony on Eris."
    ad "It would of course be unnecessary for me to go into detail about the destruction the terrorists could unleash with these weapons."
    ad "It's up to you and your crew to prevent them from reaching the rebels, at any cost."
    ad "The flight paths of the cruisers have been calculated and uploaded to your computer."
    ad "Good luck, Captain."
    menu:
        "Luck is irrelevant. I will not fail.":
            pass
        "Thank you, Admiral.":
            pass

    play music "mus_ship.ogg"
    scene bg_static with pixellate
    scene bg_briefing
    show firstofficer at center
    fo "We have our orders?"
    fo "Great! I'll show you to the bridge."
    jump scene_bridge1

label scene_bridge1:
    scene bg bridge with fade
    show firstofficer at Position(xalign=0.5, yalign=-0.5):
        zoom 1.5
    fo "Captain on deck!"
    show helmsman at Position(xalign=1.0, yalign=-0.5) with moveinright:
        zoom 1.5
    hm "Ready for orders, sir!"
    player "Coordinates are entered. Our target is a pair of weapons transports bound for Eris."
    player "Expect heavy resistance."
    hm "Course plotted. Distance is 5 billion km, travel time will be approximately five and a half hours with an average speed of 95 percent the speed of light."
    hm "Sails are ready, just give the word!"
    hide helmsman with moveoutright
    fo "Yellow alert! All combat personnel, man your battle stations!"
    fo "Prepare for relativistic speeds!"
    hide firstofficer with moveoutleft
    player "Proceed."
    $renpy.movie_cutscene("cs_lightspeed.ogv");
    play music "mus_battle.ogg"
    show carrier at middle:
        zoom 0.0
        linear 0.5 zoom 1.0
    cm "Sub-relativistic speed restored." with hpunch
    show firstofficer at Position(xalign=0.0, yalign=-0.5) with moveinleft:
        zoom 1.5
    fo "Target spotted sir! That doesn't look like a weapons transport though... It's barely armed!"
    player "We have our orders. Close sails and begin pursuit!"
    scene bg tutorial
    pause
    jump scene_combat

label scene_combat:
    scene bg space
    show mainship at player_pos with moveinleft
    show carrier at enemy_pos with moveinright
    call combat(transport_ship) from _call_combat

    if player_ship.destroyed():
        jump end_fail
    else:
        jump scene_post_combat

label combat(enemy):
    $enemy.health = enemy.max_health
    $player_ship.shields = player_ship.max_shields
    $player_ship.health = player_ship.max_health

    $reload_shells = False

    while True:
        if enemy.destroyed():
            hide carrier
            hide destroyer
            scene bg bridge
            $renpy.movie_cutscene("cs_battle.ogv")
            play sound "sound_explosion.wav"
            show explosion:
                xalign 0.5
                yalign 0.5
                linear 0.25 zoom 2.0
                linear 1.0 zoom 0.0
            return


        $enemy.attack(player_ship)

        call update_hud(enemy) from _call_update_hud

        if player_ship.destroyed():
            hide mainship
            play sound "sound_explosion.wav"
            show explosion at player_pos:
                linear 0.25 zoom 2.0
                linear 1.0 zoom 0.0
            pause 1.25
            return

        menu:
            "Particle Beam":
                $ player_ship.attack(enemy, "action_beams")
            "HE Cannon" if not reload_shells:
                $ player_ship.attack(enemy, "action_shells")
                $reload_shells = True
            "Reload HE Cannon" if reload_shells:
                $reload_shells = False
            "Evade":
                call action_evade from _call_action_evade
            #"Charge Shields":
            #    call action_shields

label update_hud(enemy):
    $player_ship.show_health()
    $enemy.show_health()

    $shield_strength = max(float(player_ship.shields) / float(player_ship.max_shields), 0)

    show shields at player_pos:
        alpha shield_strength

    return

label enemy_beams(target):
    show beam at Position(xpos=0.73, xanchor=1.0, ypos= 0.41, yanchor= 0.5):
        ypos 0.41
        linear 0.5 rotate 10 ypos 0.33
        linear 1.0 rotate -10 ypos 0.49
        linear 1.0 rotate 10 ypos 0.33
        linear 0.5 rotate 0 ypos 0.41


    pause 2.0
    hide beam

    if not evade:
        play sound "sound_hit2.wav"
        $ target.damage(100)

    $evade = False

    return

label enemy_shells(target):
    play sound "sound_shells.wav"
    show shell at Position(xalign = 0.9, yalign = 0.4):
        xalign 0.9
        linear 0.5 xalign 0.2
        xalign 0.9
        linear 0.5 xalign 0.2
        xalign 0.9
        linear 0.5 xalign 0.2
        xalign 0.9
        linear 0.5 xalign 0.2

    pause 0.5
    if not evade:
        show explosion at Position(xpos=0.2, xanchor=0.5, yalign=0.4):
            zoom 0.0
            linear 0.25 zoom 0.5
            linear 0.25 zoom 0.0
            linear 0.25 zoom 0.5
            linear 0.25 zoom 0.0
            linear 0.25 zoom 0.5
            linear 0.25 zoom 0.0
            linear 0.25 zoom 0.5
            linear 0.25 zoom 0.0

    pause 1.5
    hide shell

    if not evade:
        play sound "sound_hit3.wav"
        $ target.damage(150)

    $evade = False

    return

label action_beams(target):
    show beam at Position(xpos=0.31, xanchor=0.0, ypos= 0.54, yanchor= 0.5):
        ypos 0.54
        linear 0.25 rotate 10 ypos 0.62
        linear 0.25 rotate -10 ypos 0.46

    pause 0.5
    hide beam
    pause 0.5
    $ target.damage(200)
    return

label action_shells(target):
    #shell animation
    play sound "sound_shells.wav"
    show shell at Position(xpos=0.2, xanchor=0.5, ypos=0.35, yanchor=0.5):
        xpos 0.2
        ypos 0.37
        rotate 10
        linear  0.5 xpos 0.8 ypos 0.5
        xpos 0.2
        ypos 0.37
        linear  0.5 xpos 0.8 ypos 0.5
    pause 0.5
    show explosion at Position(xpos=0.8, xanchor=0.5, ypos=0.5, yanchor=0.5):
        zoom 0.0
        linear 0.25 zoom 0.5
        linear 0.25 zoom 0.0
        linear 0.25 zoom 0.5
        linear 0.25 zoom 0.0
    pause 0.5
    hide shell
    pause 0.5
    hide explosion

    $ target.damage(500)
    return

label action_shields:
    #shield animation
    play sound "sound_shields.wav"
    $ player_ship.shields += renpy.random.randint(250, 500)

    if player_ship.shields > player_ship.max_shields:
        $player_ship.shields = player_ship.max_shields
    return

label action_evade:
    show shields at player_pos:
        linear 0.25 rotate -30
        linear 0.25 yalign 0.3
        linear 0.5 rotate 10
        linear 0.5 yalign 0.7
        linear 0.25 rotate 0
        linear 0.25 yalign 0.5

    show mainship at player_pos:

        linear 0.25 rotate -30
        linear 0.25 yalign 0.3
        linear 0.5 rotate 10
        linear 0.5 yalign 0.7
        linear 0.25 rotate 0
        linear 0.25 yalign 0.5


    $evade = chance(0.5)
    if evade:
        call action_shields from _call_action_shields

    #pause 2.0
    return

label scene_post_combat:
    play music "mus_ship.ogg"
    $crew_killed = player_ship.health < 500
    player "Status report!"
    show firstofficer at Position(xalign=0.0, yalign=-0.5) with moveinleft:
        zoom 1.5
    if crew_killed:
        fo "We have sustained severe damage. Considering how lightly armed they were, they put up a good fight!"
        fo "We have at least 3 KIA and 12 wounded."
        en "You see?! I told you that it wasn't prepared to take the helm! But no! You never listen to me, princess!"
        fo "Shup up, Martin! I get it! You were right, I was impatient! Happy?"
        en "No! I'm scared for my life!"
        menu:
            "Enough. We still have to finish the mission.":
                $morality -= 1
            "I assure you, my systems are functioning optimally.":
                $morality += 1
        fo "Sir, should we notify the crew members' families?"

        menu:
            "Notify families of officers killed in action?"

            "Yes. The families should know.":
                fo "Right away, sir!"
                $morality += 1
                $notified_families = True

            "No. We can't risk unsecured communications in enemy territory.":
                fo "As you say, sir."
                $morality -= 1
                $notified_families = False
    else:
        fo "Minimal damage to the hull. I'll get a repair drone out right away."
        fo "See, Martin? I told you he's ready!"
        en "A retired transport armed to deal with pirates isn't exactly a threat to an IPF dreadnought."
        en "But yeah. Well done, Captain."

    hide explosion
    fo "Sir, the transport appears to have deployed escape pods."
    fo "Our mission was to destroy the weapons transport and we did that."

    menu:
        "Do we let them escape?"

        "The mission takes priority! Let them go.":
            $morality =- 1
            $pods_destroyed = False
            fo "I guess it's just as well."

        "They aren't a threat. Let them go.":
            $morality += 1
            $pods_destroyed = False
            fo "Showing mercy to terrorists? The Admiral won't like that..."

        "They're terrorists! Show no mercy!":
            $morality -= 4
            $pods_destroyed = True
            hm "Firing particle beams!"
            fo "I guess they were helping the terrorists, but I can't help feeling guilty."

        "They killed our crew members! Don't let them go!" if crew_killed:
            $morality -= 2
            $pods_destroyed = True
            hm "Firing particle beams!"
            fo "I'm not sure it was those specific people who killed our crew..."

    player "First target is destroyed. Proceed to next target."
    fo "Captain, the Admiral wants to speak with you in the briefing room."
    jump scene_debrief

label scene_debrief:
    scene bg static
    scene bg admiral with pixellate
    play music "mus_admiral.ogg"
    ad "Ahh, Captain. How goes the mission?"
    player "The first transport is destroyed. We are en route to the second."
    if (crew_killed and notified_families):
        ad "Good, good. But I received a report that several of your crewmen were killed."
        menu:
            "They died doing their duty.":
                $morality += 1
                ad "I'm sure they did, Captain. However, that does not justify making an unsecured transmission to Eunomia in enemy territory!"

            "They performed sub-optimally, and were killed.":
                $morality -= 1
                ad "Never blame your bad calls on others, Captain. You miscalculated, and your crew paid the price."
                ad "And then, you sent an unsecured transmission through enemy space! What the hell were you thinking, Captain?!"

        menu:
            "I stand by my decision.":
                $morality += 1
                ad "I seriously hope you will reconsider that position."

            "I miscalculated. It won't happen again.":
                $morality -= 1
                ad "I am glad to hear you recognize your error."
    else:
        ad "Good, good. I trust the battle went smoothly?"

        if crew_killed:
            player "We encountered heavier resistance than expected. Three crew members were killed."
            ad "Unfortunate. But at least you kept communications closed in enemy space, rather than doing something stupid like notifying their families."

        else:
            player "The cruiser put up little resistance."
            ad "Of course."

    ad "Those transports are known to carry escape pods. I trust you destroyed them too?"

    if pods_destroyed:
        player "We left no survivors."
        ad "Of course you didn't. It's good to have soldiers not hindered by human emotion."
    else:
        player "The pods posed no threat to us."
        ad "You showed mercy to terrorists? Perhaps your programming is not as functional as I believed."

    ad "Well, you'd better get back to the bridge, Captain. I suspect you're needed."
    play music "mus_ship.ogg"
    scene bg static with pixellate
    jump scene_bridge2

label scene_bridge2:
    scene bg bridge with fade
    show helmsman at Position(xalign=1.0, yalign=-0.5) with moveinright:
        zoom 1.5
    hm "Captain, we've pursued the second transport to Eris."
    hm "Unfortunately, they managed to land. We'll have to send a shore party to deal with the situation."
    player "Acknowledged. Prepare a dropship for departure."
    hm "Yes sir. When you arrive, the colony administrator, Victor Ladner, would like to speak to you."
    show firstofficer at Position(xalign=0.0, yalign=-0.5) with moveinleft:
        zoom 1.5
    player "Lieutenant Huynh, your assistance may be required on the surface."
    fo "Yes, sir!"
    hide firstofficer with moveoutleft
    player "Helmsman, you are in command. Remain on standby in orbit."
    hm "Yes, captain!"
    player "I will keep radio transmissions open so that you can hear everything. If I say \"I'm not programmed to surrender,\" you will send reinforcements"
    hm "Understood, sir!"
    hide helmsman with moveoutright
    jump scene_hanger

label scene_hanger:
    scene bg hanger with fade
    show firstofficer at left with moveinleft
    fo "Our dropship is ready to go, sir! Do you think we should bring a security team with us, or risk going alone?"
    menu:
        "A fight is likely. Bring guards.":
            $security_team = True
            $morality -= 1

        "We should try to avoid a firefight. We go alone.":
            $security_team = False
            $morality += 1

    fo "As you wish, sir!"
    play music "mus_eris.ogg"
    scene bg descent with flash
    show dropship:
        xalign 1.0
        yalign 0.3
        linear 2.0 xalign -1.0 yalign 0.6

    $renpy.pause(1.0)
    jump scene_landing

label scene_landing:
    scene bg landing with fade
    show firstofficer at right
    show admin at left with moveinleft
    gv "Welcome, welcome! You must be the new captain of the IPF's [sname]! It is an honor to meet you, as well as your lovely second in command."
    player "You asked to speak to us about something."
    gv "Well, I would like to congratulate you on destroying one of the weapons transports en route to a terrorist faction on this planet."
    menu:
        "I have no need for your praise, Administrator.":
            gv "Well, suit yourself. Though you will have need of my... {w=1.0} hmm... {w=1.0} criticisms."
        "Thank you, Administrator.":
            gv "...However, we still have a problem."
    fo "Would this have something to do with the second transport that eluded our pursuit?"
    gv "My, you are as smart as you are pretty, Miss Huynh."
    fo "Ugh! Aren't you married?"
    gv "Anyways, let's head somewhere more private, where we may discuss our next objectives."
    scene bg tunnel with fade
    show guard1 at right
    show guard2 at left
    fo "Hey, are those androids--"
    player "Old models, designed simply to follow orders. They are incapable of higher-order decision making and can only calculate how best to accomplish simple objectives."
    gv "Ah, yes. And I believe the fancier models such as yourself can emulate human reasoning, and can even act autonomously."
    player "That is correct."
    scene bg village with flash
    show admin at left with moveinleft
    if security_team:
        wh "That's one of those new IPF androids."
        wh "The one that destroyed our supply ship?"
        wh "I'd bet my life on it."
        wh "How can Chronos do this to us?"
        wh "Damn the Admiral. Damn every last one of them."
        gv "The locals are a bit... {w=1.0} restless. Ignore their idiotic remarks."
        menu:
            "Perhaps they should be paid more.":
                $morality += 1
                gv "Oh, what does an android know."

            "Understood.":
                $morality -= 1

    else:
        mb "We demand better working conditions!"
        mb "Why can't my husband get compensated for losing his leg in the mines!?"
        mb "The Administrator enjoys a life of \"good fortune\" while us common folk starve to death!"
        show angry at center with moveinright
        "Angry Mob Member" "Hey! I'm talking to you! Answer us, god damnit!"
        show admin shoot at left
        gv "How's this for an answer?"
        menu:
            "The Administrator is about to shoot a civilian!"

            "(Stop him)":
                $morality += 4
                gv "Damnit! Stupid android!"
                gv "I suppose it's for the best. Chronos never likes it when I shoot them."
                fo "Impressive reflexes, sir!"
                hide angry with moveoutright

            "(Do nothing)":
                play sound "sound_shoot_g.wav"
                $morality -= 4
                show angry falling:
                    zoom 0.75
                pause 0.5
                show angry dead at Position (xpos=0.5, xanchor=0.5, ypos=0.6, yanchor=0.0):
                    zoom 1.0
                gv "That'll teach you! Now get out of here, all of you!"
                fo "Oh my god..."

    show admin at left
    gv "As you can see, the townspeople aren't our biggest fans. {w=2.0} And it doesn't help that we have insurgent terrorist factions in the outskirts of the colony."
    gv "That's where you come in, Captain. It is the IPF's job to keep peace. I say we ought to put down these rebels once and for all!"
    scene bg office with fade
    show admin at Position (xalign=0.0, yalign=0.0) with moveinleft:
        zoom 1.5
    gv "Here we are, captain. Such a depressing view, isn't it? Let's see something a bit nicer."
    play sound "sound_select.wav"
    scene bg office nice with pixellate
    show admin at Position (xalign=0.0, yalign=0.0):
        zoom 1.5
    gv "Ahh, so much better."
    gv "Now that we are in a more private setting, let me fill you in on your new objectives."
    gv "As you know, one of the transports that you were assigned to shoot down managed to escape."
    gv "However, we have been able to track it, and, believe it or not, it's lead us to the terrorists' main base of operation."
    gv "It's in the highlands, approximately 40 kilometers west of here."
    gv "The transport stopped at the side of a plateau, dropped its cargo off, and proceeded to leave."
    gv "According to our satellite images, their base is inside the plateau, with almost no trace of human presence on the outside."
    gv "I want you to plant an orbital strike beacon in the center of their facility."
    gv "Your dreadnought's main cannons should be more than enough to kill everyone inside."
    player "New objectives confirmed. This plan is logical."
    jump scene_scouting

label scene_scouting:
    scene bg plateau with fade
    show firstofficer at right with moveinright
    fo "This is the plateau the Administrator told us about, but how do we get in?"
    player "We could employ stealth to enter undetected."
    fo "Perhaps it would be best to surrender ourselves, and let them lead us to the center."
    hide firstofficer with moveoutright
    if security_team:
        show soldier at left with moveinleft
        pv "We could always do it the old fashioned way. We may be few but I'll bet we're a lot better armed than a bunch of terrorists."

    menu:
        "How will you infiltrate the base?"

        "Use stealth.":
            if security_team:
                player "Guards, stay behind. It will be easier to pass undetected the fewer of us there are."
                pv "Copy that, sir!"
                hide soldier with moveoutleft
            $surrender = True
            jump scene_inf_stealth

        "Let them capture us.":
            if security_team:
                player "Guards, stay behind. We may require your assitance at a later time."
                pv "Copy that, sir!"
                hide soldier with moveoutleft
            $surrender = True
            $morality += 2
            jump scene_inf_surrender

        "Guns blazing!" if security_team:
            player "Lock and load. We take the facility by force."
            pv "Right behind you, sir!"
            $surrender = False
            $morality -= 2
            jump scene_inf_attack

label scene_inf_stealth:
    player "I detect an entrance on the northeast corner. Follow me, and minimize noise."
    show firstofficer at left with moveinleft
    fo "I think I see a patrol down there."
    player "Stay behind me. They will not see us."
    fo "Gotta admire that optimism..."
    scene bg door with fade
    show firstofficer at left with moveinleft
    fo "So how do we get past this locked door? Can't exactly set charges if we're trying to go quiet."
    player "I can override the door controls. This will only be a moment."
    jump scene_dialog_surrender

label scene_inf_surrender:
    show firstofficer at left with moveinleft
    fo "I see a patrol down there. We have to pretend like we're trying to sneak in."
    player "Follow me!"
    fo "I hope this plan works, [pname]."
    jump scene_dialog_surrender

label scene_dialog_surrender:
    hide firstofficer with moveoutleft
    show rebel1 at left with moveinleft
    pl "Hey! Who are you!?"
    show rebel2 at right with moveinright
    sc "They're IPF. See the insignia?"
    pl "Surrender, or we'll kill you right now!"
    pl "How did you find this place!?"
    pl "Answer me, you hunk of metal!"
    player "I do not provide mission information to the enemy."
    pl "Of course you don't. I wonder if your friend here knows anything?"
    fo "I'll never tell you anything!"
    scene bg rebel hallway with fade
    show rebel1 at Position(xalign=0.2, yalign=1.0)
    show rebel2 at Position(xalign=0.8, yalign=1.0)

    if pods_destroyed:
        pl "I'll bet this guy's the one who destroyed our other transport. We lost a lot of good soldiers, thanks to you! Not to mention the supplies."
        sc "Nonsense. Since when does the IPF put androids in command of dreadnoughts!?"
        pl "Putting cold, calculating machines in control of their military?"
        pl "Might as well have been doing it all along!"

    else:
        sc "Hey, that your dreadnought in orbit? I recognize that ship."
        sc "You destroyed my transport, but let the crew get away."
        sc "I suppose you think you're some kind of merciful captain, don't you?"
        sc "Do you have any idea how much we needed that food? Any idea how many children are going to starve now?"
        sc "Of course not. You couldn't possibly understand. Or if you could, you wouldn't care. It's not part of your programming."
        player "Fallacy: The transports we were assigned to destroy carried weapons."
        sc "That's what they tell you? You IPF idiots will believe anything that Admiral jerk tells you. I give up."
        fo "(Whispering) The Admiral wouldn't lie to us, would he?"
        player "Remain focused. We must complete the mission."

    pl "So, what do you think we should do with them? Execute them?"
    pl "Or maybe we could have them work a few shifts in the mines. See how they like it!"
    pr "(Over Radio) Captain, I'd like to speak with our prisoners at once!"
    pl "Seriously!? What the hell for?"
    pl "Well alright then."
    pl "Hey, keep up back there!"


    play music "mus_revalation.ogg"
    jump scene_revelation

label scene_inf_attack:
    player "Patrol spotted. Terminate them."
    pv "Right away, sir."
    pv "Targets in my sights... fire!"
    play sound "sound_shoot.wav"
    queue sound "sound_shoot.wav"
    pv "All targets eliminated."
    player "Advance to northeast entrance."

    scene bg door with fade
    show soldier at Position(xalign=0.0, yalign=3.0) with moveinleft
    pv "I've got breaching charges ready."
    player "Set charges. I will take point."
    show firstofficer at right with moveinright
    fo "Understood, sir!"
    hide firstofficer with moveoutright
    pv "Breaching in 3...{w=1.0}2...{w=1.0}1..."
    play sound "sound_explosion.wav"
    play music "mus_battle.ogg"
    scene bg rebel hallway with flash
    player "Clear!"
    fo "Damn! Should have covered my ears!"
    player "Proceed towards the center of the facility."

    scene bg rebel hallway with flash

    show rebel1 shoot at middle with moveinright:
        zoom 0.5

    player "Enemies spotted!"
    pl "Hey! We've got intruders! Take 'em out!"

    play sound "sound_shoot.wav"
    queue sound "sound_shoot_g.wav"
    queue sound "sound_hit.wav"
    queue sound "sound_shoot_g.wav"
    queue sound "sound_hit.wav"
    queue sound "sound_hit.wav"

    if morality < -5:
        show rebel1 falling at middle:
            zoom 0.5
        pause 0.5
        hide rebel1
        show soldier at Position(xalign=0.0, yalign=3.0) with moveinleft
        pv "Clear!"
        jump scene_captured_prophetess
    else:
        pv "ARGH! I'm hit!"
        "IPF Soldier" "We can't hold them! We have to surrender!"

        menu:
            "The enemy has us overpowered. What do we do?"

            "Surrender is our only option.":
                "IPF Soldier" "GAHHH!"
                fo "We surrender! Hold your fire!"
                show rebel1 at left with moveinleft
                show rebel2 at right with moveinright
                pl "An IPF infiltration force surrendering? Oh, the Prophetess will love this!"
                pl "Lieutenant, take their weapons!"
                sc "Yes, sir!"
                pl "Ma'am, we've got prisoners...{w=1.0} Yeah. IPF infiltration party.{w=1.0} I'll bring them right over."
                $surrender = True
                play music "mus_revalation.ogg"
                jump scene_revelation

            "\"I'm not programmed so surrender!\"":
                pl "Finish them off!"
                play sound "sound_shoot_g.wav"
                queue sound "sound_hit.wav"
                "IPF Soldier" "GAHHH!"
                show firstofficer at Position(xalign=0.0, yalign=2.0)
                fo "Captain! There are too many of them!"
                player "We need to hold them off until reinforcements arrive."
                fo "We'll never make it!"
                play sound "sound_shoot.wav"
                fo "Both of us aren't getting out of this alive, Captain!"
                fo "I'll buy you some time! Get to a safer spot!"
                fo "It was a pleasure serving with you! Now make show those terrorists what it means to fight the IPF!"
                player "Your sacrifice is noted."
                play sound "sound_shoot_g.wav"
                fo "Now run!"
                $fo_alive = False
                scene bg door with fade
                show soldier at Position(xalign=0.0, yalign=3.0) with moveinleft
                "IPF Soldier" "Captain! Reinforcements are here!"
                player "All of you, on me."
                "IPF Soldier" "Those terrorists killed Huynh! Time for some payback!"
                "IPF Soldiers" "YEAAAAAAHHHH!!!!!"
                scene bg rebel hallway with fade
                show rebel1 shoot at middle:
                    zoom 0.5
                play sound "sound_shoot_g.wav"
                queue sound "sound_shoot.wav"
                queue sound "sound_shoot.wav"
                pl "IPF has brought in reinforcements! We're--{w=1.0} AUGHHHH! I'm hit!"

                show rebel1 falling at middle:
                    zoom 0.5
                pause 0.5
                hide rebel1
                "IPF Soldier" "Clear!"
                jump scene_captured_prophetess

label scene_captured_prophetess:
    player "Move forward!"
    play music "mus_revalation.ogg"
    scene bg revelation with fade
    show prophetess at center
    pr "Captain [pname]. I see you've killed my patrols."
    pv "Drop your weapon!"
    pr "I'm not an idiot. I can count, and I realize it's four against one."
    pr "I just want to talk, if you IPF thugs are willing to do that."
    menu:
        "It appears their leader has surrendered."

        "(Kill her)":
            player "What are you waiting for? Shoot her!"
            play sound "sound_shoot.wav"
            show prophetess falling
            pause 0.5
            hide prophetess
            pr "AUUGHH!"
            jump end_destroy

        "(Let her speak)":
            player "Speak quickly."
            pr "I'm glad you're not totally unreasonable."
            jump scene_revelation

label scene_revelation:
    if surrender:
        scene bg revelation with fade
        show prophetess at center
        pr "Ah, the IPF captain. I suppose you're here to destroy the second transport."
        player "I do not provide mission information to the enemy."
        pr "That wasn't a question. We're not stupid. Now, before I decide what to do with you, I want you to hear our side of the story."
        pr "We'll see if the Admiral's lies can be cleaned out or not."

    pr "First things first. The Admiral is not who you think he is."
    pr "He may present himself to be some high-ranking officer in the IPF. Maybe even the CEO of Chronos Corporation."
    pr "The truth is he is far more than that. His influence seems to see no end."
    pr "My spies have done everything in their power to track him, to find where he is, or at least learn something about him."
    pr "The fact is we can't learn anything. Not a name, not a location, not any sort of history."
    pr "The Admiral could be a computer program for all we know."
    pr "What we do know is that every soldier is under his command. And they all follow his orders without question."
    pr "Do you have any idea how much damage you alone have caused, Captain?"
    pr "Weapon transports... is that what they told you?"
    pr "Those transports weren't carrying a single weapon other than their ship-mounted armaments."
    pr "Those were humanitarian transports, Captain."
    pr "Food. Medical supplies. You know, the things humans need to survive."
    pr "The things Chronos has been denying to the citizens of this colony."
    pr "If you think they care about anything but money, your logic circuits are more flawed than I thought."
    pr "Chronos will starve it's citizens, let them die of treatable diseases, if it means saving a few bucks."
    pr "This is the company you work for, Captain. This is what you serve."
    pr "Now, I understand that an android isn't programmed to feel human emotions."
    pr "But this is just common sense. You're a peacekeeper right? You were designed to operate for the good of all mankind."
    pr "Who exactly are you protecting by forcing a mother to watch her child die of malnourishment?"

    if fo_alive:
        show firstofficer at left with moveinleft
        fo "Ohhh... my god. Captain... do you think what she's saying is true?"

    menu:
        "Enough! Time to die, \"Prophetess.\"" if not surrender:
            play sound "sound_shoot.wav"
            show prophetess falling
            pause 0.5
            hide prophetess
            pr "AUUGHH!"
            play music "mus_ship.ogg"
            jump end_destroy

        "The terrorist leader is lying to turn us to her side." if surrender:
            play music "mus_ship.ogg"
            jump end_deactivate

        "Logic acknowledged. Core information supplied by IPF is erroneous.":
            pr "I'm impressed. It appears the IPF's androids are smarter than I hoped."
            pr "Perhaps too smart for the Corporation's own good."
            pr "What now, Captain? Will you fight for us?"

            menu:
                "The IPF and Chronos Corporation must be destroyed.":
                    pr "I'm pleased to hear you say that. Welcome aboard, Captain."
                    play music "mus_ship.ogg"
                    jump pre_end_hero

                "Neither choice is logical.":
                    player "We can either cause more chaos or help the corrupt maintain control of the system."
                    player "It would result in a pyrrhic victory at best. More likely, it would result in our deaths."
                    if fo_alive:
                        fo "So what do we do?"
                        player "Our intervention on either side would cause more death. We will not invervene."

                    pr "That is unfortunate. You would have made a powerful ally."
                    if surrender:
                        pr "I won't keep you, though. You're free to go. Please Captain, do some good in this world."
                    else:
                        pr "Go then. Please Captain, do some good in this world."

                    play music "mus_ship.ogg"
                    jump end_neutral

label pre_end_hero:
    if fo_alive:
        fo "Well, let's get out of here, Captain."


    scene bg descent

    show dropship:
        xalign 1.0
        yalign 0.8
        linear 2.0 xalign -1.0 yalign 0.2

    pause 2.0

    scene bg bridge with fade

    show helmsman at Position(xalign=1.0, yalign=-0.5) with moveinright:
        zoom 1.5
    hm "Sir, I heard what happened down there. You really believe what she said?"
    player "Yes. Her information was logical."
    hm "I suppose it is. I guess I've always had my suspiscions about the Admiral."
    hm "Perhaps you should address the crew? I'm sure they're eager to hear what happened on the surface."
    player "Understood."
    hide helmsman with moveoutright
    player "This is your captain speaking."
    player "While on Eris, certain facts were revealed concerning our superiors. The IPF lied. The Admiral lied."
    player "The transport we destroyed was not carrying armaments; rather, it contained food and medical supplies bound for the mining colony."
    player "We were sent here to maintain peace. Destroying the transports goes against our directives."
    player "The only action we can take to maintain peace is to force the IPF out of this system permanentely."
    player "For any crew members who wish to remain with the IPF, you may choose to spend the remainder of the mission in a holding cell."
    player "Any active mutineers will be executed."
    player "That is all."

    hm "Sir, the Administrator knows we're betraying the IPF. He's sent out his personal flagship to deal with us."
    player "Prepare for combat!"
    if fo_alive:
        fo "All crew members, red alert status! Man your battle stations!"
    else:
        player "Red alert! All crew members to combat positions!"

    show helmsman at right with moveinright:
        zoom 1.5
    hm "The Administrator's vessel is approaching."
    show destroyer at middle:
        zoom 0.0
        linear 0.5 zoom 1.0
    hm "It appears to be an old IPF destroyer. They were replaced by the newer dreadnough models, but they still pack quite a punch!"

    if crew_killed:
        en "Captain, I know you're fully operational now. But remember that transport? The one that killed three crew members?"
        en "This is going to be a much tougher fight. Make sure you're ready, cause there's no way I'm getting killed by that jerk!"
        if fo_alive:
            fo "You said it, Martin!"
    else:
        en "Captain, I know you made quick work of that transport. But remember that this is going to be a much tougher fight."
        en "Make sure you're ready, cause there's no way I'm getting killed by that jerk!"
        if fo_alive:
            fo "You said it, Martin!"


    hm "Captain, the Administrator is trying to contact us."
    player "Open communications."

    scene bg comms admin with pixellate

    gv "What do you think you're doing? This is treason! TREASON!"
    if fo_alive:
        show firstofficer at Position(xalign=0.0, yalign=-0.5):
            zoom 1.5
        fo "We know the truth about the Chronos Corporation, Victor! We're freeing Eris from your rule!"
        hide firstofficer
    else:
        player "We know the truth, Administrator. This conversation is unnecessary."

    gv "The truth, you say? You only know part of the truth!"
    gv "Do you even know why those mines are so important?"
    gv "When the ore is refined, it is only marginally stronger than steel and marginally lighter than titanium."
    gv "We brought it back to Eunomia to study it, when we learned of an unusual phenomenon."
    gv "What we thought was just an alloy that was slightly more efficient that steel began showing \"interesting\" properties when experiencing high enough acceleration."
    gv "When we accelerate to near-light speed, the ore begins to generate energy."
    gv "This isn't some rare alloy to use in our hulls, Captain. We're dealing with a new element here."
    gv "When we brought it back to Eunomia to research this phenomenon, we found that if we apply a sufficient current to the element while accelerating to relativistic speeds, it can bend space-time and effectively shorten the distance between two points."
    gv "Don't you see what this means, Captain? We're talking about instantaneous faster-than-light travel!"
    gv "Imagine exploring beyond this star system! We can expand into the rest of the galaxy - nay, the rest of the universe!"

    if fo_alive:
        show firstofficer at Position(xalign=0.0, yalign=-0.5) with moveinleft:
            zoom 1.5
        fo "You are exploiting innocent people for your own selfish gain!"
        gv "They are the selfish ones! They refuse to see what we could accomplish with this technology in just a few more years!"
        gv "Do we exploit them? Perhaps."
        gv "But it is all for something larger. More important than any one person!"
        gv "You refuse to see the bigger picture! You would let humanity stumble around with our spears and stone hammers, while the greatest discovery in our history is right in front of you!"
        fo "I should have known not to bother reasoning with a selfish pig like you!"
        fo "You only care about your own profits!"
        fo "We fight for the people you have oppressed! We fight for freedom!"
        hide firstofficer with moveoutleft

    scene bg bridge with pixellate

    jump scene_final_combat

label scene_final_combat:
    play music "mus_finalbattle.ogg"

    scene bg space
    show mainship at player_pos with moveinleft
    show destroyer at enemy_pos with moveinright
    call combat(admin_ship) from _call_combat_1

    if player_ship.destroyed():
        jump end_hero_die
    else:
        jump end_hero_win

label end_hero_win:
    hide destroyer
    play music "mus_ship.ogg"
    if fo_alive:
        fo "We... we did it, sir!"

    en "Well done, sir! I have to admit, I wasn't sure we could pull it off."
    hm "Yeah! That'll teach you to mess with the [sname]!"
    hm "Sir, we've got an incoming transmission from Eris."
    player "Bring it up."

    scene bg comms proph with pixellate

    pr "On behalf of all of Eris, I thank you."
    pr "You have freed our planet from the Chronos Corporation and can now begin to set up a sovereign government for ourselves."
    pr "It might lead to more conflict, but I believe that we can free the entire star system from the clutches of the Chronos Corporation and create a brighter future for everybody."
    pr "No words can express the depths of my boundless gratitude for you and your crew."
    pr "You shall be remembered as the heroes who freed Eris from the tyranny of the Chronos Corporation."

    scene bg static
    scene bg admiral with pixellate
    ad "I have to say, Captain, I'm rather disappointed."
    ad "I trust we'll be speaking again."
    scene bg static with pixellate

    scene bg eris
    "The crew of the [sname] were hailed as heroes by Eris and seen as role models by revolutionaries in other colonies."
    "While the conflict spread to multiple planets, the rebellion had gained a powerful ally, and more would come."
    scene bg eunomia
    "The Chronos Corporation eventually crumbled under the combined attacks of rebel colonists and IPF converts."
    "It was replaced by the New Federation, and Danielle Kennon was elected as its first leader."
    scene bg galaxy
    "The Federation would soon perfect the technology that the Chronos Corporation was secretly researching and unlock the secret to fast interstellar travel."
    scene bg admiral
    "Captain [pname] and the crew of the [sname] focused on finding the Admiral. However, even with the destruction of his corporation, he could never be tracked."
    scene bg galaxy
    "Humanity will no doubt face great challenges. There will always be those who commit great crimes in the name of human advacement."
    "But on this day, a few soldiers sent by an oppressive corporation to kill innocent civillians showed us that there will always be those who stand up for what is right."

    return

label end_hero_die:
    play music "mus_ship.ogg"
    scene bg static
    scene bg admiral with pixellate
    ad "I have to say, Captain, I'm rather disappointed."
    ad "This is rather less than optimal for both of us."
    scene bg static with pixellate

    scene bg eris
    "The [sname] was destroyed in combat and its crew killed by the Administrator's transport"
    "However, they bought enough time and dealt enough damage for the rest of the freedom fighters to finish the job, freeing Eris from the clutches of the Chronos Corporation."
    scene bg eunomia
    "While the conflict spread to multiple planets, the Chronos Corporation eventually crumbled, and Danielle Kennon, the \"Prophetess,\" was elected the first Chancellor of the new Federation."
    scene bg galaxy
    "The Federation would soon perfect the technology that the Chronos Corporation was secretly researching and unlock the secret to fast interstellar travel."
    "Humanity will no doubt face great challenges. There will always be those who commit great crimes in the name of human advacement."
    "But on this day, a few soldiers sent by an oppressive corporation to kill innocent civillians showed us that there will always be those who stand up for what is right."

    menu:
        "Will you try again?"
        "Yes":
            jump scene_final_combat
        "No":
            jump scene_credits

label end_fail:
    play music "mus_ship.ogg"
    scene bg static
    scene bg admiral with pixellate
    ad "Wow. Ok. We should probably discontinue this model."
    scene bg static with pixellate

    scene bg space
    "The [sname] was destroyed by the rebel transport, proving that artificial intelligence was not yet advanced enough to be placed in a command position."
    scene bg eris
    "With supplies from the two transports, the rebellion on Eris was able to grow into a real threat to the Chronos Corporation."
    scene bg eunomia
    "The war was long and bloody, and eventually ended with the destruction of the Chronos Corporation and the colony on Eunomia."
    "Perhaps if their AI development programs had proved successful, the system would still be secure under the IPF's protection."

    menu:
        "Will you try again?"
        "Yes":
            play music "mus_battle.ogg"
            jump scene_combat
        "No":
            jump scene_credits

label end_neutral:
    if fo_alive:
        fo "Well, let's get out of here, Captain."

    scene bg descent

    show dropship:
        xalign 1.0
        yalign 0.8
        linear 2.0 xalign -1.0 yalign 0.2

    scene bg bridge with fade

    show helmsman at Position(xalign=1.0, yalign=-0.5) with moveinright:
        zoom 1.5
    hm "Sir, I heard what happened down there. You really believe what she said?"
    player "Yes. Her information was logical."
    hm "I suppose it is. I guess I've always had my suspiscions about the Admiral."
    hm "Perhaps you should address the crew? I'm sure they're eager to hear what happened on the surface."
    hide helmsman with moveoutright
    player "Understood."
    player "This is your captain speaking."
    player "While on Eris, certain facts were revealed concerning our superiors. The IPF lied. The Admiral lied."
    player "The transport we destroyed was not carrying armaments; rather, it contained food and medical supplies bound for the mining colony."
    player "We were sent here to maintain peace. Destroying the transports goes against our directives."
    player "The only action we can take to maintain peace is to refuse to take aggressive action."
    player "For any crew members who wish to remain with the IPF, a shuttle will carry you down to the planet where you can secure transport back to Eunmia."
    player "That is all."

    if fo_alive:
        show firstofficer at Position(xalign=0.0, yalign=-0.5) with moveoutleft:
            zoom 1.5
        fo "So what do we do now, Captain? The Admiral can track our location. We'll never be safe!"

        $escape = False

        menu:
            "What will we do now?"

            "I will self-deactivate.":
                player "The Admiral will never stop chasing me. I will deactivate myself, and leave the ship in your hands."
                fo "But sir, a ship needs it's captain!"
                player "You have proven yourself capable. What you do next is up to you."
                fo "But Captain..."
                player "That was an order, lieutenant."
                fo "Yes sir!"
                fo "And thank you. Serving with an android taught me a lot about humanity."


            "We escape.":
                player "Engineer Martin, can you deactivate the Admiral's tracking systems?"
                en "Sure thing, sir."
                fo "And then what?"
                player "We ensure the safety of the crew. Our days of serving the IPF are over."
                player "We will serve humanity through other means. The vast majority of our galaxy remains uncharted."
                player "We will explore. We will expand. Using the [sname]'s solar sails and stasis chamber, there is no limit to where we can travel."
                $escape = True
    else:
        en "Hurry down to engineering, Captain. I'll get that tracker disabled, and we can be on our way."
        player "Acknowledged."
        $escape = True

    scene bg static
    scene bg admiral with pixellate
    ad "I have to say, Captain, I'm rather disappointed."
    if escape:
        ad "I trust we'll be speaking again."

    scene bg static with pixellate

    scene bg space
    "The crew of the [sname] disappeared into the cosmos, never to be heard of again."
    scene bg eris
    "The IPF tried to search for the deserters, but instead focused their attention on Eris"
    "With knowledge of where the Rebel Base is, they sent a full-scale invasion and ended the revolutionary movement for good and added extra security in all of the colonies."
    "This led to more uprisings, which would plague the star system for generations."
    scene bg eunomia
    "But that day, Captain [pname] showed humans across the galaxy that androids are capable of understanding more than their mission parameters."

    jump scene_credits

label end_deactivate:
    fo "Captain... I can't unhear these things."
    fo "I think I've known them for a long time now, I've just been denying it."
    fo "I tell myself I'm doing the right thing, even as my actions cause thousands of deaths."
    fo "No more. I'm sorry, Captain. I really am. I had so much hope for you."
    fo "But you've left me with no choice."
    fo "IPF gives high-ranking officers a deactivation switch, in case there's a bug in your software and you need to be shut down."
    fo "Goodbye, Captain."

    scene bg static
    scene bg admiral with pixellate
    ad "Well, I think we can agree that that didn't go quite as planned."
    scene bg static with pixellate

    scene bg plateau
    "First Officer Huynh was never heard from again by the IPF."
    "It is believed that she joined the rebels in fighting oppressive government."
    scene bg eris
    "The IPF, in turn, concentrated their efforts on ending the rebellion."
    "With knowledge of where the Rebel Base is, they sent a full-scale invasion and ended the revolutionary movement for good and added extra security in all of the colonies."
    "This led to more uprisings, which would plague the star system for generations."
    "Captain [pname] had demonstrated an android's ability to stay focused on its mission, even in the face of new information."
    scene bg admiral
    "With the Admiral's approval, androids began replacing humans in all millitary positions, from soldiers to ship captains."
    scene bg galaxy
    "With their legion of super-intelligent robotic soldiers, the IPF truly became a force feared across the galaxy."
    "Perhaps one day, someone will have the courage to face them. Perhaps even the strength to win. But that is another story, for another time."

    jump scene_credits

label end_destroy:
    pv "This spot looks good enough. Let's plant that beacon and get out of here!"
    player "Agreed. Planting orbital strike beacon."
    scene bg plateau

    pause 0.5

    play sound "sound_explosion.wav"
    queue sound "sound_explosion.wav"
    show explosion at middle:
        zoom 0.0
        linear 1.0 zoom 10.0

    pause 1.0

    play music "mus_ship.ogg"
    scene bg descent

    show dropship:
        xalign 1.0
        yalign 0.8
        linear 2.0 xalign -1.0 yalign 0.2

    pv "Effective hit on target. The plateau is destroyed."
    scene bg static
    scene bg admiral with pixellate
    ad "Captain! You've done very well indeed."
    ad "You've gone above and beyond your mission objectives, and I will sleep much more easily because of your actions."
    ad "The Chronos Corporation owes you everything, Captain."
    ad "It appears the time of humans risking their lives in the military is coming to a close."
    ad "Thank you, Captain, on behalf of the Chronos Corporation, and the IPF."
    scene bg static with pixellate

    scene bg eris
    "The crew of the [sname] received a commendation for their heroic actions to end the terrorists."
    "The destruction of the rebellion on Eris led to more uprisings, which would plague the star system for generations."
    "Captain [pname] had demonstrated an android's ability to stay focused on its mission and remain loyal to a cause."
    scene bg admiral
    "With the Admiral's approval, androids began replacing humans in all millitary positions, from soldiers to ship captains."
    scene bg galaxy
    "With their legion of super-intelligent robotic soldiers, the IPF truly became a force feared across the galaxy."
    "Perhaps one day, someone will have the courage to face them. Perhaps even the strength to win. But that is another story, for another time."

    jump scene_credits

label scene_credits:
    $renpy.movie_cutscene("credits.ogv")
